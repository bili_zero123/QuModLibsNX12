# -*- coding: utf-8 -*-
from ....Server import serverApi, Events, levelId, _loaderSystem
from ..Server import BaseService, BaseEvent
from Globals import _ItemBasicInfo, _ItemData, _InventoryData
lambda: "物品服务模块 By Zero123"
lambda: "TIME: 2024/05/06"

class ItemBasicInfo(_ItemBasicInfo):
    """ 基础物品信息 """
    def getArgs(self, itemName, auxValue, isEnchanted):
        # type: (str, int, bool) -> dict
        comp = serverApi.GetEngineCompFactory().CreateItem(levelId)
        return comp.GetItemBasicInfo(itemName, auxValue, isEnchanted)

class ItemData(_ItemData):
    """ 物品参数 """
    def getItemBasicInfo(self):
        # type: () -> _ItemBasicInfo
        """ 获取物品基础信息 """
        return ItemBasicInfo(self.newItemName, self.newAuxValue)

class InventoryData(_InventoryData):
    """ 背包物品信息 """
    USE_ITEM_CLS = ItemData

class BaseItemService(BaseService):
    """ 基本物品服务 """
    @staticmethod
    def spawnInventoryItems(_inventoryData, pos, dm):
        # type: (InventoryData, tuple[float], int) -> None
        """ 将一个背包中的所有物品生成到世界 """
        for itemData in _inventoryData.walk():
            if itemData.empty:
                continue
            _loaderSystem.CreateEngineItemEntity(itemData.getDict(), dm, tuple(pos))

    @staticmethod
    def getPlayerInventoryData(playerId, getUserData = False, itemPosType = 0):
        """ 获取玩家背包物品信息 itemPosType默认为背包 getUserData启用后将拿到userData字段 """
        comp = serverApi.GetEngineCompFactory().CreateItem(playerId)
        return InventoryData.loadItemDictList(comp.GetPlayerAllItems(itemPosType, getUserData), playerId)

    @staticmethod
    def setPlayerInventoryData(playerId, inventoryData, itemPosType = 0, leaveBlankValues = False):
        # type: (str, InventoryData, int, bool) -> None
        """ 设置玩家背包物品信息 itemPosType默认为背包 leaveBlankValues启用后将会保留空物品对象 """
        comp = serverApi.GetEngineCompFactory().CreateItem(playerId)
        comp.SetPlayerAllItems(inventoryData.getItemsDictMap(itemPosType, leaveBlankValues))

    @staticmethod
    def setEntityInventoryData(entityId, data, itemPosType = 0, leaveBlankValues = False):
        # type: (str, InventoryData | ItemData, int, bool) -> None
        """ 设置实体背包物品信息 itemPosType默认为背包 leaveBlankValues启用后将会保留空物品对象 """
        comp = serverApi.GetEngineCompFactory().CreateItem(entityId)
        if isinstance(data, InventoryData):
            for itemData in data.walk():
                if not leaveBlankValues and itemData.empty:
                    continue
                comp.SetEntityItem(itemPosType, itemData.getDict(), itemData.index)
        elif isinstance(data, ItemData):
            if not leaveBlankValues and data.empty:
                return
            comp.SetEntityItem(itemPosType, data.getDict(), 0)

    @staticmethod
    def setPlayerHandItem(playerId, handItemData):
        # type: (str, ItemData) -> None
        """ 设置玩家手持物品 """
        comp = serverApi.GetEngineCompFactory().CreateItem(playerId)
        comp.SpawnItemToPlayerCarried(handItemData.getDict(), playerId)
    
    @staticmethod
    def getPlayerHandItem(playerId, getUserData = False):
        # type: (str, bool) -> ItemData
        """ 获取玩家手持物品 """
        comp = serverApi.GetEngineCompFactory().CreateItem(playerId)
        return ItemData(comp.GetPlayerItem(2, 0, getUserData), playerId)
    
    def changePlayerHandItem(self, playerId, handItemData):
        """ 改变玩家手中物品 使用此方法不会触发相关事件 若需触发请使用静态方法setPlayerHandItem """
        from time import time
        self._needPass = True
        self._passEventTime += time() + 0.0001
        BaseItemService.setPlayerHandItem(playerId, handItemData)

    class PlayerHoldNewItemEvent(BaseEvent):
        """ 玩家手持了新物品触发 """
        def __init__(self, oldItemData, newItemData, playerId = None):
            # type: (ItemData, ItemData, str | None) -> None
            BaseEvent.__init__(self)
            self.playerId = playerId
            self.newItemData = newItemData
            self.oldItemData = oldItemData
            self.needUpdate = False
            """ 是否需要更新新物品表现 仅支持newItemData 设置为True将会在事件完毕后自动执行 """
        
        def updateItemData(self):
            """ 立即更新事件发生后的物品参数 将影响到实际切换物品表现 仅支持newItemData """
            comp = serverApi.GetEngineCompFactory().CreateItem(self.playerId)
            comp.SpawnItemToPlayerCarried(self.newItemData.getDict(), self.playerId)

    def __init__(self):
        BaseService.__init__(self)
        if self.__class__ is BaseItemService:
            raise Exception("禁止实例化基类服务")
        self._passEventTime = 0.0
        self._needPass = False
    
    def PROCESSING_JUDGMENT(self, itemData):
        # type: (ItemData) -> bool
        """ 该方法用于请求判定是否需要计算物品事件 默认对全部物品触发 """
        return True

    @BaseService.Listen(Events.OnCarriedNewItemChangedServerEvent)
    def OnCarriedNewItemChangedServerEvent(self, args={}):
        """ 玩家切换手持物品触发 """
        playerId = args["playerId"]
        oldItemData = ItemData(args["oldItemDict"], playerId)
        newItemData = ItemData(args["newItemDict"], playerId)
        if not (self.PROCESSING_JUDGMENT(oldItemData) or self.PROCESSING_JUDGMENT(newItemData)):
            # 新旧物品中若都不符合计算判定条件 则终止业务
            return
        if self._needPass:
            from time import time
            self._needPass = False
            if time() <= self._passEventTime:
                return
        eventData = BaseItemService.PlayerHoldNewItemEvent(oldItemData, newItemData, playerId)
        self.broadcast(
            eventData
        )
        if eventData.needUpdate:
            eventData.updateItemData()