# -*- coding: utf-8 -*-
Version = "1.2.0-NX (Beta)"                                     # 版本信息 : str
ApiVersion = 3                                                  # API版本 : int
Author = "Zero123"                                              # 创作者: str
ContactInformation = "QQ:913702423"                             # 联系方式: str
Other = """
    # QuModLibs By Zero123(网易:游趣开发组) 别名:一灵 | h2v-wither123... BilBil-UID:456549011
    # 开源协议: BSD (适用于我们在Gitee/Github等渠道上公布的版本)
    # 附加条例: 使用QuModLibs开发的项目如需商用, 应在发布页标明使用的相关框架及涉及到的[三方扩展](如有需要)

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
"""


"""
NX12更新内容:
    1.新增 Business 业务类 处理子业务逻辑分支 并提供 AnnotationLoader, TimerLoader类 定制注解/自有定时任务处理
    2.新增GLRender渲染系统 采用高版本API 为玩家资源操作提供更加现代化的解决方案
    3.新增ItemData, InventoryData面向对象的物理, 背包资源管理
    4.新增WeightUtil权重工具模块 提供QWeightSelector, QWeightPool类 以便随机抽取权重结果
    5.Math模块 Vec3新增dot运算
    6.ATE战斗扩展新增基于GLR渲染系统的 PyAttackBusiness, PyBattleExpansion 派生类
    7.ATE战斗扩展新增 ACParameter, AC_AttackData 封装类
    8.ATEDefineParam类提供宏 USE_GLR_LOAD_QUERY 启用后将采用GLR新渲染系统处理玩家节点
    9.新增 Modules.UI 扩展模块 提供QGridData, QUICanvas类以便高效的处理分页UI及动态网格渲染
    10.新增 Modules.UI.Anims 扩展模块 提供UI动画系统管理
    11.新增 Modules.UI.Funcs 扩展模块 提供UI功能扩展

    FIX:
        - 修复NX11版本中 服务端/客户端不注册任何内容时退出游戏发生的资源错误问题
    
    功能限制:
        ATE模块受商务纠纷暂时不对外开放(最初为三方开发者定做的功能)
        未来或将解除限制
    
    功能废弃(在未来):
        - 旧的UI动画系统将在未来彻底移除 取而代之的是Modules.UI.Anims
        - PublicData类将在未来彻底移除
"""