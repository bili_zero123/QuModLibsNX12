# QuModLibsNX12_Beta

#### 介绍
适用于网易MCMOD开发的免费开源框架 (如需摘录部分源代码到其他项目请署名原作者)
交流反馈群: Q494731530

#### NX12新增内容
1.新增 Business 业务类 处理子业务逻辑分支 并提供 AnnotationLoader, TimerLoader类 定制注解/自有定时任务处理

2.新增GLRender渲染系统 采用高版本API 为玩家资源操作提供更加现代化的解决方案

3.新增ItemData, InventoryData面向对象的物理, 背包资源管理

4.新增WeightUtil权重工具模块 提供QWeightSelector, QWeightPool类 以便随机抽取权重结果

5.Math模块 Vec3新增dot运算

6.ATE战斗扩展新增基于GLR渲染系统的 PyAttackBusiness, PyBattleExpansion 派生类

7.ATE战斗扩展新增 ACParameter, AC_AttackData 封装类

8.ATEDefineParam类提供宏 USE_GLR_LOAD_QUERY 启用后将采用GLR新渲染系统处理玩家节点

9.新增 Modules.UI 扩展模块 提供QGridData, QUICanvas类以便高效的处理分页UI及动态网格渲染

10.新增 Modules.UI.Anims 扩展模块 提供UI动画系统管理

11.新增 Modules.UI.Funcs 扩展模块 提供UI功能扩展

#### 修复内容
- 修复NX11版本中 服务端/客户端不注册任何内容时退出游戏发生的资源错误问题

#### 功能限制
ATE模块受商务纠纷暂时不对外开放(最初为三方开发者定做的功能)
未来或将解除限制

#### 功能废弃(在未来)
- 旧的UI动画系统将在未来彻底移除 取而代之的是Modules.UI.Anims
- PublicData类将在未来彻底移除
